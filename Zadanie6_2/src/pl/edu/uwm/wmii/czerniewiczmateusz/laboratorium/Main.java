package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {

    public static void main(String[] args) {
        IntegerSet a = new IntegerSet();
        IntegerSet b = new IntegerSet();

        a.insertElement(5);
        a.insertElement(27);
        a.insertElement(18);
        a.insertElement(54);
        a.insertElement(98);
        b.insertElement(18);
        b.insertElement(5);
        b.insertElement(34);
        b.insertElement(47);
        b.insertElement(73);
        System.out.println("a: "+a.toString());
        System.out.println("b: "+b.toString());
        System.out.println("--------------");

        IntegerSet u = new IntegerSet();
        System.out.println("Suma: "+u.union(a,b));
        IntegerSet i = new IntegerSet();
        System.out.println("Iloczyn: " +i.intersection(a,b));
        System.out.println("Czy są równe? " + a.equals(b.tab));
        a.deleteElement(27);
        a.deleteElement(54);
        a.deleteElement(98);
        b.deleteElement(34);
        b.deleteElement(47);
        b.deleteElement(73);
        System.out.println("a: "+a.toString());
        System.out.println("b: "+b.toString());








    }
}
