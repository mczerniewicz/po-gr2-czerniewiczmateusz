package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.lang.reflect.Array;
import java.util.*;
import java.io.*;
public class IntegerSet {
    boolean[] tab;

    public IntegerSet()
    {
        this.tab=new boolean[100];
    }
    public static IntegerSet union(IntegerSet set1,IntegerSet set2)
    {
        IntegerSet set = new IntegerSet();
        for(int i=0;i<set1.tab.length;i++)
        {
            set.tab[i]=set1.tab[i];
        }
        int counter = set1.tab.length;
        for(int i=0;i< 100;i++)
        {
            if(set.tab[i]!=set2.tab[i]&&set.tab[i]==false)
            {
                set.tab[i]=set2.tab[i];
            }
        }
        return set;
    }
    public static IntegerSet intersection(IntegerSet set1,IntegerSet set2)
    {
        IntegerSet set = new IntegerSet();
        for(int i=0;i<100;i++)
        {
            if(set1.tab[i]==set2.tab[i])
            {
                set.tab[i]=set1.tab[i];
            }
        }
        return set;
    }
    public void insertElement(int n)
    {
        tab[n-1]=true;
    }
    public void deleteElement(int n)
    {
        tab[n-1]=false;
    }
    public String toString()
    {
        String wynik;
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        int counter = 0;
       for(int i=0;i<100;i++)
       {
           if(tab[i])
           {
               tmp.add(i+1);
           }
       }
       wynik=tmp.toString();
       return wynik;
    }
    public boolean equals(boolean[] tab1)
    {
        if(tab.equals(tab1))
        {
            return true;
        }
        else {
            return false;
        }
    }


}
