package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class PairUtilDemo {

    public static void main(String[] args)
    {
        String[] dane = { "Mateusz","Nowak" };
        PairUtil<String> d = new PairUtil<String>();
        Pair<String> p = new Pair<String>();
        p.setFirst(dane[0]);
        p.setSecond(dane[1]);
        System.out.println(p.getFirst());
        System.out.println(p.getSecond());
        Pair<String> nowa = d.swap(p);
        System.out.println(nowa.getFirst());
        System.out.println(nowa.getSecond());

    }
}
