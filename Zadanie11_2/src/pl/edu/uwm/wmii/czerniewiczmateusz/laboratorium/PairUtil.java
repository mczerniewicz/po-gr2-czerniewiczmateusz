package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class PairUtil<T> extends Pair{

    public static <T> Pair swap(Pair<T> p)
    {
        T tmp = p.getFirst();
        p.setFirst(p.getSecond());
        p.setSecond(tmp);
        return p;
    }

}
