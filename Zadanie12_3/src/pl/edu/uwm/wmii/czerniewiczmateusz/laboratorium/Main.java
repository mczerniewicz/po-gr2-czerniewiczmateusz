package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<String> a = new LinkedList<>();
        a.add("Ola");
        a.add("Adam");
        a.add("Szymon");
        a.add("Kuba");
        a.add("Wojtek");
        a.add("Patryk");
        a.add("Kamil");
        System.out.println(a);
        odwroc(a);
        System.out.println(a);
    }
    public static void odwroc(LinkedList<String> lista)
    {
        LinkedList<String> tmp = new LinkedList<>();
        for(int i = lista.size()-1;i>=0;i--)
        {
            tmp.add(lista.get(i));
        }
        lista.clear();
        for(int i=0;i<tmp.size();i++)
        {
            lista.add(tmp.get(i));
        }
    }

}
