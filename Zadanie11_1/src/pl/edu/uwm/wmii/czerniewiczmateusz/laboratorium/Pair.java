package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Pair<T> {

    public Pair() {
        first = null;
        second = null;
    }

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public void setTmp(T tmp) {
        this.tmp = tmp;
    }

    public T getTmp() {
        return tmp;
    }

    public <T> void swap()
    {
        setTmp(getFirst());
        setFirst(getSecond());
        setSecond(getTmp());
    }


    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    private T first;
    private T second;
    private T tmp;
}

