package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class PairDemo {

    public static void main(String[] args)
    {
        String[] dane = { "Mateusz","Nowak" };
        Pair<String> d = new Pair<String>();
        d.setFirst(dane[0]);
        d.setSecond(dane[1]);
        System.out.println(d.getFirst());
        System.out.println(d.getSecond());
        d.swap();
        System.out.println(d.getFirst());
        System.out.println(d.getSecond());

    }
}



