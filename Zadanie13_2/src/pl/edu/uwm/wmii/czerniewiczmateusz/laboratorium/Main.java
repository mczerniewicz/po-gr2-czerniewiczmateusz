package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<char[], char[]> stud = new HashMap<>();
        System.out.println("Witaj w liście studentów. Wpisz odpowiednie komendy aby wykonać akcje:");
        System.out.println("zakończ - kończy program");
        System.out.println("dodaj - dodaje studenta");
        System.out.println("usuń - usuwa studenta o podanym nazwisku");
        System.out.println("wypisz - wypisuje listę studentów");
        String odpowiedz = "odp";

        while (!odpowiedz.equals("zakończ")) {
            odpowiedz = scan.nextLine();
            switch (odpowiedz) {
                case "dodaj":

                    System.out.println("Podaj nazwisko: ");
                    String nazwisko = scan.next();
                    System.out.println("Podaj ocene: ");
                    String ocena = scan.next();
                    stud.put(nazwisko.toCharArray(), ocena.toCharArray());
                    odpowiedz = "odp";
                    System.out.println("Dodano " + nazwisko + " " + ocena);

                    break;
                case "usuń":

                    System.out.print("Podaj nazwisko: ");
                    String nazw = scan.next();
                    stud.remove(nazw);
                    System.out.println(nazw);
                    odpowiedz = "odp";
                    break;

                case "wypisz":

                    Set<char[]> nazwiska = stud.keySet();
                    for(char[] elem : nazwiska)
                    {
                        wypisz(elem);
                        System.out.print(" : ");
                        wypisz(stud.get(elem));
                        System.out.println();
                    }
                    odpowiedz = "odp";
                    break;
                case "zakończ":
                    break;

            }
        }
    }
    public static void wypisz(char[] a){
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i]);
        }
    }
}

