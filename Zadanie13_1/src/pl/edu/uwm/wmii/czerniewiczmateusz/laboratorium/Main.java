package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<Integer, String> zadania = new TreeMap<>();
        System.out.println("Witaj w liście zadań. Wpisz odpowiednie komendy aby wykonać akcje:");
        System.out.println("zakończ - kończy program");
        System.out.println("dodaj - dodaje kolejne zadanie");
        System.out.println("następne - usuwa zadanie z największym priorytetem");
        System.out.println("wypisz - wypisuje aktualne zadania i ich priorytety");
        String odpowiedz = "odp";

        while (!odpowiedz.equals("zakończ")) {
            odpowiedz = scan.nextLine();
            switch (odpowiedz) {
                case "dodaj":

                    System.out.println("Podaj priorytet: ");
                    int prio = scan.nextInt();
                    System.out.println("Podaj opis: ");
                    String opis = scan.next();
                    zadania.put(prio, opis);
                    odpowiedz = "odp";
                    System.out.println("Dodano zadanie " + opis);

                    break;
                case "następne":

                    Set<Integer> klucze = zadania.keySet();
                    zadania.remove(klucze.iterator().next());
                    odpowiedz = "odp";
                    break;
                case "wypisz":

                    System.out.println(zadania);
                    odpowiedz = "odp";
                    break;
                case "zakończ":
                    break;

                }
            }
        }
    }

