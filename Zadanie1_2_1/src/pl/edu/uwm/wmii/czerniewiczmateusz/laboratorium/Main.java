package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {

    public static void main(String[] args) {
	int[] tablica = new int[]{1,2,3,4,5,6,7,8,9,10};
	int n = tablica.length;
	System.out.println(a(n,tablica));
	System.out.println(b(n,tablica));
	System.out.println(c(n,tablica));
	System.out.println(d(n,tablica));
	System.out.println(e(n,tablica));
	System.out.println(f(n,tablica));
	System.out.println(g(n,tablica));
	System.out.println(h(n,tablica));

    }
    public static int a(int n, int[] tab){
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if(tab[i]%2==1)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int b(int n, int[] tab){
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if(tab[i]%3==0&&tab[i]%5!=0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int c(int n, int[] tab){
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if(Math.sqrt(tab[i])%2==0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int d(int n, int[] tab){
        int wynik=0;
        for(int i=1;i<n-1;i++)
        {
            if(tab[i]<(tab[i-1]+tab[i+1])/2)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int e(int n, int[] tab){
        int wynik=0;
        int tmp=1;
        for(int i=0;i<n;i++)
        {
            tmp*=i+1;
            if(tab[i]>Math.pow(2,i)&&tab[i]<tmp)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int f(int n, int[] tab){
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if((i+1)%2==1&&tab[i]%2==0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int g(int n, int[] tab){
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if(tab[i]%2==1&&tab[i]>=0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int h(int n, int[] tab){
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if(Math.abs(tab[i])<Math.pow(i+1,2))
            {
                wynik++;
            }
        }
        return wynik;
    }
}
