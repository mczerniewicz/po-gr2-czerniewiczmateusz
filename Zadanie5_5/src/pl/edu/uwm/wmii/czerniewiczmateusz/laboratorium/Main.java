package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import javax.lang.model.type.NullType;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> a= create(5);
        System.out.println("a= "+a);
        reverse(a);
        System.out.println("a reversed= "+a);
    }
    public static void reverse(ArrayList<Integer> a)
    {
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        for(int i=0;i<a.size();i++)
        {
            tmp.add(a.get(i));
        }
        a.clear();
        for(int i=0;i<tmp.size();i++)
        {
            a.add(tmp.get(tmp.size()-1-i));
        }
    }
    public static ArrayList<Integer> create(int dl)
    {
        ArrayList<Integer> wynik= new ArrayList<Integer>();
        for(int i=0;i<dl;i++)
        {
            wynik.add((int)(Math.random()*50));
        }
        return wynik;
    }



}
