package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Integer[] cyfry1 = new Integer[]{1,2,3,4,5,6};
        Integer[] cyfry2 = new Integer[]{1,2,0,4,5,6};
        LocalDate a = LocalDate.of(2000,1,1);
        LocalDate b = LocalDate.of(2000,1,2);
        LocalDate c = LocalDate.of(2000,1,3);
        LocalDate[] daty1 = new LocalDate[]{a,b,c};
        LocalDate[] daty2 = new LocalDate[]{b,a,c};
        ArrayUtil test = new ArrayUtil();
        test.wypisz(cyfry1);
        test.wypisz(cyfry2);
        test.wypisz(daty1);
        test.wypisz(daty2);
        cyfry1=test.selectionSort(cyfry1);
        cyfry2=test.selectionSort(cyfry2);
        test.wypisz(cyfry1);
        test.wypisz(cyfry2);


    }
}

