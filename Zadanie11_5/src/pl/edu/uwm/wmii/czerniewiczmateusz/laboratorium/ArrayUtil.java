package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class ArrayUtil<T> {
    public static <T extends Comparable> T[] selectionSort(T[] tab)
    {
        Integer min;
        T tmp;
        for(int i=0;i<tab.length;i++)
        {
            min=(int)tab[i];
            for(int j=i;j<tab.length;j++)
            {
                if(tab[j].compareTo(min)==-1)
                {
                    min=(Integer)tab[j];
                    tmp=tab[i];
                    tab[i]=(T)min;
                    tab[j]=tmp;
                }
            }



        }
        return tab;
    }
    public static <T> void wypisz(T[] tab)
    {
        for(int i=0;i< tab.length;i++)
        {
            System.out.print(tab[i]+" ");
        }
        System.out.println("");
    }



}
