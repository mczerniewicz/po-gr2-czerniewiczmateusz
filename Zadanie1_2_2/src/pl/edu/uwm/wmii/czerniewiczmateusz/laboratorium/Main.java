package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {

    public static void main(String[] args) {
	double[] tablica= new double[]{-3,-2,-1,0,1,2,3,4,5};
	int n = tablica.length;
	System.out.println(podw_suma(n,tablica));

    }
    public static double podw_suma(int n, double[] tab){
        double wynik=0;
        for(int i=0;i<n;i++)
        {
            if(tab[i]>0)
            {
                wynik+=tab[i];
            }
        }
        return 2*wynik;
    }
}
