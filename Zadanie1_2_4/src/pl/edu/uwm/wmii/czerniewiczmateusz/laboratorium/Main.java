package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {

    public static void main(String[] args) {
        double[] tablica= new double[]{-3,-2,-1,0,1,2,3,4,5};
        int n = tablica.length;
        System.out.println("Minimum= "+minmax(n,tablica)[0]);
        System.out.println("Maximum= "+minmax(n,tablica)[1]);

    }

    public static double[] minmax(int n, double[] tab){
        double max=tab[0];
        double min=tab[0];
        for(int i=1;i<n;i++)
        {
            if(tab[i]<min)
            {
                min=tab[i];
            }
            if(tab[i]>max)
            {
                max=tab[i];
            }
        }
        double[] wynik= new double[]{min,max};
        return wynik;
    }

}
