package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę od 1 do 100:");
        int n = in.nextInt();

        int tab[] = new int[n];
        generuj(-999,999,n,tab);
        wypisz(tab);
        System.out.println("Parzyste: "+a(tab)[0]);
        System.out.println("Nieparzyste: "+a(tab)[1]);
        System.out.println("Ujemne: "+b(tab)[0]);
        System.out.println("Zera: "+b(tab)[1]);
        System.out.println("Dodatnie: "+b(tab)[2]);
        System.out.println("Największy: "+c(tab)[0]);
        System.out.println("Występowanie: "+c(tab)[1]);
        System.out.println("Ujemne suma: "+d(tab)[0]);
        System.out.println("Dodatnie suma: "+d(tab)[1]);
        System.out.println("Najdłuższy ciąg dodatnich: "+e(tab));
        System.out.println("Dodatnie na 1 i ujemne na -1: ");
        f(tab);wypisz(tab);
        System.out.println("Zamiana od 2 do 5 elementu: ");
        g(3,6,tab);
        wypisz(tab);









    }
    public static int[] a(int[] tab)
    {
        int p = 0;
        int np= 0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]%2==0)
            {
                p++;
            }
            else{
                np++;
            }
        }
        int[] wynik= new int[]{p,np};
        return wynik;
    }
    public static int[] b(int[] tab)
    {
        int z = 0;
        int d = 0;
        int u = 0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]==0)
            {
                z++;
            }
            if(tab[i]>0)
            {
                d++;
            }
            if(tab[i]<0)
            {
                u++;
            }
        }
        int[] wynik = new int[]{u,z,d};
        return wynik;
    }
    public static int[] c(int[] tab)
    {
        int max= tab[0];
        int ile=0;
        for(int i=1;i<tab.length;i++)
        {
            if(tab[i]>max)
            {
                max=tab[i];
            }
        }
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]==max)
            {
                ile++;
            }
        }
        int[] wynik = new int[]{max,ile};
        return wynik;
    }
    public static int[] d(int[] tab)
    {
        int d=0;
        int u=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0)
            {
                d+=tab[i];
            }
            if(tab[i]<0)
            {
                u+=tab[i];
            }
        }
        int[] wynik = new int[]{u,d};
        return wynik;
    }
    public static int e(int[] tab)
    {
        int[] wynik = new int[tab.length];
        int n=0;
        int tmp=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0)
            {
                tmp++;
            }
            else{
                wynik[n]=tmp;
                n++;
                tmp=0;
            }
        }

        return c(wynik)[0];
    }
    public static void f(int[] tab)
    {
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]<0)
            {
                tab[i]=-1;
            }
            if(tab[i]>0)
            {
                tab[i]=1;
            }
        }
        wypisz(tab);
    }
    public static void g(int lewy, int prawy, int[] tab)
    {
        int dl= prawy-lewy+1;
        int[] tmp = new int[dl];
        int c=0;
        for(int i=lewy-1;i<prawy;i++)
        {
            tmp[dl-1-c]=tab[i];
            c++;
        }
        for(int k=lewy-1;k<prawy;k++)
        {
            tab[k]=tmp[k-lewy+1];
        }
    }
    public static void generuj(int min, int max, int n, int[] tab)
    {
        int przedzial = max - min;
        for(int i=0;i<n;i++)
        {
            int r = (int) (Math.random()*przedzial)+min;
            tab[i]= r;
        }
    }

    public static void wypisz(int[] tab){
        for(int i =0;i<tab.length;i++)
        {
            System.out.println(tab[i]);
        }
    }
}
