package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<String> a = new LinkedList<>();
        LinkedList<Integer> b = new LinkedList<>();
        a.add("Ola");
        a.add("Adam");
        a.add("Szymon");
        a.add("Kuba");
        a.add("Wojtek");
        a.add("Patryk");
        a.add("Kamil");
        b.add(12);
        b.add(2);
        b.add(46);
        b.add(18645);
        b.add(2);
        b.add(331);

        System.out.println(a);
        odwroc(a);
        System.out.println(a);
        System.out.println(b);
        odwroc(b);
        System.out.println(b);

    }
    public static <T> void odwroc(LinkedList<T> lista)
    {
        LinkedList<T> tmp = new LinkedList<>();
        for(int i = lista.size()-1;i>=0;i--)
        {
            tmp.add(lista.get(i));
        }
        lista.clear();
        for(int i=0;i<tmp.size();i++)
        {
            lista.add(tmp.get(i));
        }
    }

}
