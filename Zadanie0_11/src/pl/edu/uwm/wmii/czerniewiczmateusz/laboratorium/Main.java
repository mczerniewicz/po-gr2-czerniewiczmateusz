package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {
    public static void main(String[] args){
        System.out.println("Wisława Szymborska - Na wieży Babel");
        System.out.println("- Która godzina? - Tak, jestem szczęśliwa,\n" +
                "i brak mi tylko dzwoneczka u szyi,\n" +
                "który by brzęczał nad Tobą, gdy śpisz.\n" +
                "- Więc nie słyszałaś burzy? Murem targnął wiatr,\n" +
                "wieża ziewnęła jak lew, wielką bramą\n" +
                "na skrzypiących zawiasach. - Jak to, zapomniałeś?\n" +
                "Miałam na sobie zwykłą szarą suknię\n" +
                "spinaną na ramieniu. - I natychmiast potem\n" +
                "niebo pękło w stubłysku. - Jakże mogłam wejść,\n" +
                "przecież nie byłeś sam. - Ujrzałem nagle\n" +
                "kolory sprzed istnienia wzroku. - Szkoda,\n" +
                "że nie możesz mi przyrzec. - Masz słuszność,\n" +
                "widocznie to był sen. - Dlaczego kłamiesz,\n" +
                "dlaczego mówisz do mnie jej imieniem,\n" +
                "kochasz ją jeszcze? - O tak, chciałbym,\n" +
                "żebyś została ze mną. - Nie mam żalu,\n" +
                "powinnam była domyślić się tego.\n" +
                "- Wciąż myślisz o nim? - Ależ ja nie płaczę.\n" +
                "- I to juź wszystko? - Nikogo jak ciebie.\n" +
                "- Przynajmniej jesteś szczera. - Bądź spokojny,\n" +
                "wyjadę z tego miasta. - Bądź spokojna,\n" +
                "odejdę stąd. - Masz takie piękne ręce.\n" +
                "- To stare dzieje, ostrze przeszło\n" +
                "nie naruszając kości. - Nie ma za co,\n" +
                "mój drogi, nie ma za co. - Nie wiem\n" +
                "i nie chcę wiedzieć, która to godzina.");
    }
}
