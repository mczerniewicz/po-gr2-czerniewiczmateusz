package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import pl.imiajd.czerniewicz.*;


import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {


        Pracownik a = new Pracownik("Jan Kowalski", 50000, LocalDate.of(2020,11,15));
        Student b = new Student("Małgorzata Nowak", "informatyka",4.55);
        System.out.println(a.getNazwisko()+": " + a.getOpis());
        System.out.println(a.getDataZatrudnienia());
        System.out.println(b.getNazwisko()+": " + b.getOpis());
        System.out.println(b.getSredniaOcen());
    }
}
