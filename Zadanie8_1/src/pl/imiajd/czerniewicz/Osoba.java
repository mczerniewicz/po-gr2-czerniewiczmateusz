package pl.imiajd.czerniewicz;

import java.time.LocalDate;

abstract class Osoba {
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public Osoba(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String[] getImiona() {
        return imiona;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public boolean isPlec() {
        return plec;
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    private String nazwisko;
    public String[] imiona;
    public LocalDate dataUrodzenia;
    boolean plec;
}