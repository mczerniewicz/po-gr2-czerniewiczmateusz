package pl.imiajd.czerniewicz;

import java.time.LocalDate;

public class Pracownik extends Osoba
{


    public Pracownik(String nazwisko, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }
    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }
    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    private double pobory;
    public LocalDate dataZatrudnienia;
}