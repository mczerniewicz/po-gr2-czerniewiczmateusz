package pl.imiajd.czerniewicz;

public class Student extends Osoba
{
    public Student(String nazwisko, String kierunek, double sredniaOcen)
    {
        super(nazwisko);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    private String kierunek;
    public double sredniaOcen;
}
