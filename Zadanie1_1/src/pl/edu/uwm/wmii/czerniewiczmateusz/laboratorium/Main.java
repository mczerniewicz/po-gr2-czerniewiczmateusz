package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {
    public static void main(String[] args){
        double[] tablica = new double[]{12.5, 4.17, 3, 5.44};
        int n = tablica.length;
        wypisz(tablica);
        System.out.println("Suma= "+suma(n,tablica));
        System.out.println("Iloczyn= "+iloczyn(n,tablica));
        System.out.println("Suma wartości bezwględnych= "+sumaabs(n,tablica));
        System.out.println("Suma pierwiastków= "+sumapierw(n,tablica));
        System.out.println("Iloczyn wartości bezwględnych= "+iloczynabs(n,tablica));
        System.out.println("Suma kwadratów= "+sumapow(n,tablica));
        wypisz(sumail(n,tablica));
        System.out.println("Suma z (-1)^n+1= "+sumaminus(n,tablica));
        System.out.println("Suma (a*n(-1)^n+1)/n!= "+sumasilnia(n,tablica));


    }
    public static void wypisz(double tab[]){
        for(int i=0;i<tab.length;i++)
        {
            System.out.print(tab[i]+", ");
        }
    }
    public static double suma(int n, double tab[]){
        double wynik=0;
        for(int i=0;i<n;i++){
            wynik+=tab[i];
        }
        return wynik;
    }
    public static double iloczyn(int n, double tab[]){
        double wynik=1;
        for(int i=0;i<n;i++){
            wynik*=tab[i];
        }
        return wynik;
    }
    public static double sumaabs(int n, double tab[]){
        double wynik=0;
        for(int i=0;i<n;i++){
            wynik+=Math.abs(tab[i]);
        }
        return wynik;
    }
    public static double sumapierw(int n, double tab[]){
        double wynik=0;
        for(int i=0;i<n;i++){
            wynik+=Math.sqrt(Math.abs(tab[i]));
        }
        return wynik;
    }
    public static double iloczynabs(int n, double tab[]){
        double wynik=1;
        for(int i=0;i<n;i++){
            wynik*=Math.abs(tab[i]);
        }
        return wynik;
    }
    public static double sumapow(int n, double tab[]){
        double wynik=0;
        for(int i=0;i<n;i++){
            wynik+=Math.pow(tab[i],2);
        }
        return wynik;
    }
    public static double[] sumail(int n, double tab[]){
        double wynik1 = 0;
        double wynik2 = 1;

        for(int i=0;i<n;i++){
            wynik1+=tab[i];
            wynik2*=tab[i];
        }
        double[] wynik = new double[]{wynik1,wynik2};
        return wynik;
    }
    public static double sumaminus(int n, double tab[]){
        double wynik=0;
        for(int i=0;i<n;i++){
            wynik+=tab[i]*Math.pow(-1,i+2);
        }
        return wynik;
    }
    public static double sumasilnia(int n, double tab[]){
        double wynik=0;
        int tmp = 1;
        for(int i=1;i<n+1;i++){
            tmp*=i;
            wynik+=(tab[i-1]*Math.pow(-1,i))/tmp;
        }
        return wynik;
    }

}
