package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<String> a = new LinkedList<>();
        a.add("Ola");
        a.add("Adam");
        a.add("Szymon");
        a.add("Kuba");
        a.add("Wojtek");
        a.add("Patryk");
        a.add("Kamil");
        System.out.println(a);
        redukuj(a,2);
        System.out.println(a);

    }
    public static void redukuj(LinkedList<String> pracownicy, int n)
    {
        int i = n-1;
        while(i<pracownicy.size())
        {
            pracownicy.remove(i);
            i+=n-1;
        }

    }
}
