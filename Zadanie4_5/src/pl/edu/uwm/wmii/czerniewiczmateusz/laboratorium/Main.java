package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Main {

    public static void main(String[] args) {
	    int k = 6300;
	    double p = 0.2;
	    int n = 5;
	    BigDecimal p1 = new BigDecimal(p);
	    BigDecimal a = new BigDecimal(k);
	    for(int i=0;i<n;i++)
        {
            a=a.add(a.multiply(p1));
        }
	    System.out.println(a.setScale(2, RoundingMode.CEILING));
    }
}
