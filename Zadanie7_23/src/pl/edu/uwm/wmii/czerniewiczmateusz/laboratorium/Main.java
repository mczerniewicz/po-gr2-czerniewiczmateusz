package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import pl.imiajd.czerniewicz.Adres;
public class Main {

    public static void main(String[] args) {
	    Adres a = new Adres("Dworcowa",23,25,"Olsztyn","10-560");
        Adres b = new Adres("Towarowa",56,"Olsztyn","10-310");
        a.pokaz();
        b.pokaz();
        System.out.println("Czy a jest przed b?: "+a.przed(b));
        System.out.println("Czy b jest przed a?: "+b.przed(a));

    }
}
