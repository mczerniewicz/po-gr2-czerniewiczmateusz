package pl.imiajd.czerniewicz;

public class Student extends Osoba {
    public Student(String nazwisko,int rok_urodzenia,String kierunek)
    {
        super(nazwisko,rok_urodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }

    String kierunek;

    public String toString()
    {
        return super.toString()+", "+kierunek;
    }
}
