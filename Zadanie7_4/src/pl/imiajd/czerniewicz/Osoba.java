package pl.imiajd.czerniewicz;

public class Osoba {
    public String getNazwisko() {
        return nazwisko;
    }

    public int getRok_urodzenia() {
        return rok_urodzenia;
    }

    public Osoba(String nazwisko, int rok_urodzenia)
    {
        this.nazwisko = nazwisko;
        this.rok_urodzenia = rok_urodzenia;
    }
    private String nazwisko;
    private int rok_urodzenia;
    public String toString()
    {
        return nazwisko + ", " + rok_urodzenia;
    }
}
