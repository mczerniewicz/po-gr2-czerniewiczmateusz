package pl.imiajd.czerniewicz;

public class Nauczyciel extends Osoba {
    int pensja;

    public int getPensja() {
        return pensja;
    }
    public Nauczyciel(String nazwisko, int rok_urodzenia, int pensja)
    {
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }
    public String toString()
    {
        return super.toString()+", "+pensja;
    }
}
