package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import pl.imiajd.czerniewicz.*;
public class Main {

    public static void main(String[] args) {
        Osoba a = new Osoba("Nowak",1995);
        Nauczyciel b = new Nauczyciel("Kowalski",1975,3000);
        Student c = new Student("Polski", 2000,"Informatyka");
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(c.toString());
    }
}
