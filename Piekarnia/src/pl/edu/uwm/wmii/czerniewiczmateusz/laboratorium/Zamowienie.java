package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.time.LocalDateTime;


public abstract class Zamowienie {
    protected LocalDateTime czasDostawy;
    public boolean PoprawnyCzas()
    {
        if(czasDostawy.compareTo(LocalDateTime.now())>1)
        {
            return true;
        }
        return false;
    }
    public void UstawCzasDostawy(LocalDateTime czasDostawy)
    {
        this.czasDostawy=czasDostawy;
    }

}
