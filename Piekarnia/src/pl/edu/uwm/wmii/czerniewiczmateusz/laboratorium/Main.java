package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Przepis przepis = new Przepis();
        String nazwa = "";
        while(nazwa=="") {
            System.out.println("Podaj nazwę przepisu: ");
            nazwa = scan.next();
        }
        int czas=-1;
        while(czas<0 || czas>300)
        {
            System.out.println("Podaj czas przygotowania: ");
            czas = scan.nextInt();
        }

        //dodawanie składników
        ArrayList<Ingredient> skladniki = new ArrayList<>();
        String odp = "exit";


        System.out.println("Dodaj przynajmniej 3 składniki.");
        System.out.println("Podaj ilość składników:");
        int il = scan.nextInt();
            for(int i=0;i<il;i++) {
                String nazw = "";
                while (nazw.equals("")) {
                    System.out.println("Podaj nazwę: ");
                    nazw = scan.next();
                }
                String ilosc = "";
                while (ilosc.equals("")) {
                    System.out.println("Podaj ilość: ");
                    ilosc = scan.next();
                }
                double cena = -1;
                while (cena <= 0) {
                    System.out.println("Podaj cenę: ");
                    cena = scan.nextDouble();
                }
                skladniki.add(new Ingredient(nazw, ilosc, cena));
            }


        //dostawa
        System.out.println("Wybierz rodzaj dostawy(cyfrę):\n[1]Na miejscu\n[2]Na wynos");
        switch (scan.nextInt()) {
            case 1 -> {
                NaMiejscu tmp = new NaMiejscu();

                System.out.println("Podaj datę odbioru :");
                LocalDateTime odbior;
                int rok = LocalDateTime.now().getYear();
                int mies = LocalDateTime.now().getMonthValue();
                int dzien = LocalDateTime.now().getDayOfMonth();
                System.out.println("Podaj godzinę HH");
                int godz = scan.nextInt();
                System.out.println("Podaj minuty MM");
                int min = scan.nextInt();
                tmp.UstawCzasDostawy(LocalDateTime.of(rok, mies, dzien, godz, min));
                if (!tmp.PoprawnyCzas()) {
                    System.out.println("Odbiór niemożliwy, spróbuj jeszcze raz");
                }

                else System.out.println("Odbiór możliwy");
            }
            case 2 -> {
                NaWynos tmp1 = new NaWynos();

                System.out.println("Podaj datę dostawy :");
                LocalDateTime dostawa;
                int rok = LocalDateTime.now().getYear();
                int mies = LocalDateTime.now().getMonthValue();
                int dzien = LocalDateTime.now().getDayOfMonth();
                System.out.println("Podaj godzinę HH");
                int godz = scan.nextInt();
                System.out.println("Podaj minuty MM");
                int min = scan.nextInt();
                tmp1.UstawCzasDostawy(LocalDateTime.of(rok, mies, dzien, godz, min));
                if (!tmp1.PoprawnyCzas()) {
                    System.out.println("Dostawa niemożliwa, spróbuj jeszcze raz");
                }
                else System.out.println("Dostawa możliwa");
            }
        }



    }
}
