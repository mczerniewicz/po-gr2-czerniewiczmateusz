package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.lang.*;
public class Ingredient implements Comparable{
    public Ingredient(String nazwaIngredienta, String ilosc, double cenaIngredienta)
    {
        this.nazwaIngredienta=nazwaIngredienta;
        this.ilosc=ilosc;
        this.cenaIngredienta=cenaIngredienta;
    }

    @Override
    public String toString()
    {
        return "Nazwa: '" + nazwaIngredienta + ", ilość: '" + ilosc + ", cena: " + cenaIngredienta;
    }

    public double getCenaIngredienta() {
        return cenaIngredienta;
    }


    public int compareTo(Ingredient other)
    {
        return this.nazwaIngredienta.compareTo(other.nazwaIngredienta);
    }
    private String nazwaIngredienta;
    private String ilosc;
    private double cenaIngredienta;

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
