package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.ArrayList;

public class Przepis {
    private String nazwa;
    private double suma=0;
    private ArrayList<Ingredient> ingredienci;
    private int czasPrzygotowania;
    public void DodajIngredient(String nazwa,String ilosc, double cena)
    {
        Ingredient a =new Ingredient(nazwa,ilosc,cena);
        ingredienci.add(a);
        this.suma+=cena;
    }
    public void UstawNazweICzas(String nazwa,int czas)
    {
        this.nazwa = nazwa;
        this.czasPrzygotowania = czas;
    }

    @Override
    public String toString() {
        if(ingredienci.size()>0) {
            return "Przepis:" + '\n' + ingredienci.toString() + '\n' + "Suma= " + suma;
        }
        return "";
    }
    public boolean CzyCzas()
    {
        if(czasPrzygotowania>0)
        {
            return true;
        }
        return false;
    }
    public int IleIngredientow()
    {
        return ingredienci.size();
    }

}
