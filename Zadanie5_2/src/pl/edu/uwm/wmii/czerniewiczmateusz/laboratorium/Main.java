package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> a= create(5);
        ArrayList<Integer> b= create(5);
        System.out.println("a= "+a);
        System.out.println("b= "+b);
        System.out.println("a+b= "+merge(a,b));
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a,ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik= new ArrayList<Integer>();
        int k=0;
        if(a.size()>b.size())
        {
            for(int i=0;i<b.size();i++)
            {
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }

            for(int j=b.size();j<a.size();j++)
            {
                wynik.add(a.get(j));
            }
        }
        if(a.size()<b.size())
        {
            for(int i=0;i<a.size();i++)
            {
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }

            for(int j=a.size();j<b.size();j++)
            {
                wynik.add(b.get(j));
            }
        }

        else {

            for (int i = 0; i < a.size(); i++) {
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
        }

        return wynik;
    }
    public static ArrayList<Integer> create(int dl)
    {
        ArrayList<Integer> wynik= new ArrayList<Integer>();
        for(int i=0;i<dl;i++)
        {
            wynik.add((int)(Math.random()*50));
        }
        return wynik;
    }

}
