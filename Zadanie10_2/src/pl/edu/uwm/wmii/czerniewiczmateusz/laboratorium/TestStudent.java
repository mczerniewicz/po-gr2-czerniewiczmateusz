package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import pl.imiajd.czerniewicz.Osoba;
import pl.imiajd.czerniewicz.Student;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestStudent {

    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<Student>();
        Student a = new Student("Domaszyński", LocalDate.of(2000,12,12),4.50);
        Student b = new Student("Nowak", LocalDate.of(2000,12,12),5.00);
        Student c = new Student("Domaszyński", LocalDate.of(1995,6,29),3.20);
        Student d = new Student("Domaszyński", LocalDate.of(2000,12,12),4.55);
        Student e = new Student("Czerniewicz", LocalDate.of(2000,2,13),4.50);
        grupa.add(a);
        grupa.add(b);
        grupa.add(c);
        grupa.add(d);
        grupa.add(e);

        for(int i=0;i<grupa.size();i++)
        {
            System.out.println(grupa.get(i).toString());
        }
        System.out.println("---------------------");
        grupa.sort(Student::compareTo);
        for(int i=0;i<grupa.size();i++)
        {
            System.out.println(grupa.get(i).toString());
        }
    }
}
