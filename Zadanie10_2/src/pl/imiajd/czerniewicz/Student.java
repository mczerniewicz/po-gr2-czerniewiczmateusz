package pl.imiajd.czerniewicz;
import java.time.LocalDate;
public class Student extends Osoba implements Cloneable, Comparable<Osoba> {
    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen =  sredniaOcen;
    }

    @Override
    public String toString() {
        return super.toString() + "sredniaOcen=" + sredniaOcen;
    }


    public int compareTo(Student other) {
        int wynik = super.compareTo(other);
        if(wynik==0)
        {
           if(this.sredniaOcen>other.sredniaOcen)
           {
               wynik = 1;
           }
           if(this.sredniaOcen== other.sredniaOcen)
           {
               wynik = 0;
           }
           wynik = -1;
        }
        return wynik;

    }
    private double sredniaOcen;
}
