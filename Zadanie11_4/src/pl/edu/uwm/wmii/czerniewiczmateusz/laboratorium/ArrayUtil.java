package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class ArrayUtil<T> {
    public static <T extends Comparable> T binSearch(T[] tab,T szuk){
        int l = 0;
        int p = tab.length-1;
        int sr;
        Integer count=0;
        while(l<=p)
        {
            sr = (l + p)/2;

            if(tab[sr] == szuk) {
                return (T)count;
            }

            if(tab[sr].compareTo(szuk)==-1) {
                p = sr - 1;
            }

            else {
                l = sr + 1;
            }
            count++;
        }
        count=count-count-1;
        return (T)count;

    }
}
