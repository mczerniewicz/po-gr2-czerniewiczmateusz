package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.math.BigInteger;
public class Main {

    public static void main(String[] args) {
        int n = 587;
        BigInteger a = new BigInteger("0");
        for (int i = 0; i < n * n; i++) {
            int tmp = 2^i;
            Integer in = Integer.valueOf(tmp);
            BigInteger b = BigInteger.valueOf(i);
            a=a.add(b);
        }
        System.out.println(a);
    }
}

