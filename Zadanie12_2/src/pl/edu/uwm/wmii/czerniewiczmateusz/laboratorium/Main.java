package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<String> a = new LinkedList<>();
        LinkedList<Integer> b = new LinkedList<>();
        a.add("Ola");
        a.add("Adam");
        a.add("Szymon");
        a.add("Kuba");
        a.add("Wojtek");
        a.add("Patryk");
        a.add("Kamil");
        b.add(12);
        b.add(2);
        b.add(46);
        b.add(18645);
        b.add(2);
        b.add(331);

        System.out.println(a);
        redukuj(a,3);
        System.out.println(a);
        System.out.println(b);
        redukuj(b,3);
        System.out.println(b);

    }
    public static <T> void redukuj(LinkedList<T> pracownicy, int n)
    {
        int i = n-1;
        while(i<pracownicy.size())
        {
            pracownicy.remove(i);
            i+=n-1;
        }

    }
}
