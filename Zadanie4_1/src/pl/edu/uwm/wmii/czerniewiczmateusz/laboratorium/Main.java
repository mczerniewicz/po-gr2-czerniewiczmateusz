package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
	String a = "alfabet rowerek kubełek rowerek";
	char e = 'e';
	String b = "rowerek";
    System.out.println("Ilość liter " + e + " : " + countChar(a,e));
    System.out.println("Ilość słów " + b + " : " + countSubStr(a,b));
    System.out.println("Podaj w jakim stringu podać środek: ");
    String slowo = in.nextLine();
    System.out.println("środek to:" + middle(slowo));
    System.out.println("ra"+repeat("ba",6)+"bar");
    wypisz(where(a,b));
    System.out.println(change("AlIbabA"));
    System.out.println(nice(b));
    System.out.println(nice2(b,'*',2));
    }

    public static void wypisz(int[] tab)
    {
        for(int i=0;i<tab.length;i++)
        {
            System.out.println(tab[i]);
        }
    }
    public static int countChar(String str,char c)
    {
        int wynik=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==c)
            {
                wynik++;
            }
        }
        return wynik;
    }

    public static int countSubStr(String str, String subStr)
    {
        int wynik=0;
        String[] spt = str.split(" ");
        for(int i=0;i<spt.length;i++)
        {
            if (spt[i].contains(subStr))
            {
                wynik++;
            }

        }
        return wynik;
    }

    public static String middle(String str)
    {
        int dl = str.length();
        if(dl%2==0)
        {
            return Character.toString(str.charAt(dl/2)).concat(Character.toString(str.charAt(dl/2+1)));
        }
        else{
            return Character.toString(str.charAt(dl/2));
        }
    }

    public static String repeat(String str, int n)
    {
        String tmp = str;
        for(int i=0;i<n-1;i++)
        {
            str=str.concat(tmp);
        }
        return str;
    }

    public static int[] where(String str, String subStr)
    {
        String[] spt = str.split(" ");
        int[] wynik = new int[countSubStr(str,subStr)];
        int iter=0;
        for(int i=0;i<spt.length;i++)
        {
            if (spt[i].contains(subStr))
            {
                wynik[iter]=i;
                iter++;
            }
        }
        return wynik;
    }

    public static String change(String str)
    {
        StringBuffer tmp= new StringBuffer("");
        for(int i=0;i<str.length();i++)
        {
            if(Character.isUpperCase(str.charAt(i)))
            {
                tmp.append(Character.toLowerCase(str.charAt(i)));
            }
            else{
                tmp.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        String wynik = tmp.toString();
        return wynik;
    }

    public static String nice(String str)
    {
        StringBuffer tmp = new StringBuffer("");
        int count = 0;
        int n = str.length();
        for(int i=0;i<n;i++)
        {
            if(count==3)
            {
                tmp.append("\'");
                count=0;
                i--;
            }
            else{
            int a = str.charAt(i);
            tmp.append(a);
            count++;
        }
        }
        String wynik = tmp.toString();
        return wynik;
    }

    public static String nice2(String str, char sep, int c)
    {
        StringBuffer tmp = new StringBuffer("");
        int count = 0;
        int n = str.length();
        for(int i=0;i<n;i++)
        {
            if(count==c)
            {
                tmp.append(sep);
                count=0;
                i--;
            }
            else{
                int a = str.charAt(i);
                tmp.append(a);
                count++;
            }
        }
        String wynik = tmp.toString();
        return wynik;
    }
}
