package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {

    public static void main(String[] args) {
        double[] tablica= new double[]{-3,2,1,-1,0,1,-2,3,4,7,-5};
        int n = tablica.length;
        System.out.println("Ilość par= "+pary(n,tablica));
    }
    public static int pary(int n, double[] tab)
    {
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            if(tab[i]>0&&tab[i+1]>0)
            {
                wynik++;
            }
        }
        return wynik;
    }

}
