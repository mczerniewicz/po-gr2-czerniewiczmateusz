package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import pl.imiajd.czerniewicz.*;

import java.util.ArrayList;
import java.util.ArrayList.*;
import java.time.LocalDate;

public class Main {


    public static void main(String[] args) {
	    ArrayList<Instrument> orkiestra = new ArrayList<Instrument>();
	    Flet a=new Flet("BachCompany", LocalDate.of(1996,5,3));
	    Skrzypce b=new Skrzypce("Gmol", LocalDate.of(1956,7,12));
	    Skrzypce c=new Skrzypce("Gmol", LocalDate.of(2005,10,4));
	    Fortepian d=new Fortepian("NotSoForte", LocalDate.of(1990,6,30));
	    Skrzypce e=new Skrzypce("Alle", LocalDate.of(1989,12,28));

	    orkiestra.add(a);
        orkiestra.add(b);
        orkiestra.add(c);
        orkiestra.add(d);
        orkiestra.add(e);
        a.dzwiek();
        b.dzwiek();
        c.dzwiek();
        d.dzwiek();
        e.dzwiek();

        for(int i =0;i<orkiestra.size();i++)
        {
            System.out.println(orkiestra.get(i).toString());
        }


    }
}
