package pl.imiajd.czerniewicz;
import java.time.LocalDate;
import java.util.Objects;

abstract public class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    abstract public void dzwiek();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrument that = (Instrument) o;
        return Objects.equals(producent, that.producent) &&
                Objects.equals(rokProdukcji, that.rokProdukcji);
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "producent='" + producent + '\'' +
                ", rokProdukcji=" + rokProdukcji +
                '}';
    }

    public String producent;
    public LocalDate rokProdukcji;




}
//Zdefiniuj abstrakcyjną klasę Instrument.
// W klasie tej zadeklaruj pole producent typu String oraz rokProdukcji typu java.time.LocalDate.
// Zdefiniuj także metodę typu get,abstrakcyjną metodę dzwiek oraz zaimplementuj metody equals i toString.
// Następnie zdefiniuj trzy klasy:Flet,Fortepian oraz Skrzypce, wszystkie dziedziczące po klasie Instrument i
// implementującę metodę dzwiek. Każdą z wymienionych klas umieść w osobnym pliku w pakiecie o
// nazwiepl.imiajd.nazwisko, gdzie nazwisko to Twoje nazwisko bez polskich liter.
// Napisz program TestInstrumenty, w którym utwórz listę 5 instrumentów o nazwie orkiestra posługując się
// klasą ListArray. W składzie orkiestry powinien wystapić przynajmniej raz instrument każdego rodzaju.
// Dla każdego instrumentu z orkiestry wywołaj metodę dzwiek po czym wyświetl zawartość listy orkiestra.