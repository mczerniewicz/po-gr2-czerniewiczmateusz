package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import static pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium.RachunekBankowy.setRocznaStopaProcentowa;

public class Main {

    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000,0.04);
        RachunekBankowy saver2 = new RachunekBankowy(3000,0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Saver1: " + saver1.getSaldo());
        System.out.println("Saver2: " + saver2.getSaldo());
        saver1.setRocznaStopaProcentowa(0.05);
        saver2.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Saver1: " + saver1.getSaldo());
        System.out.println("Saver2: " + saver2.getSaldo());




    }
}
