package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class RachunekBankowy {
    public RachunekBankowy(double saldo, double rocznaStopaProcentowa)
    {
        this.saldo = saldo;
        this.rocznaStopaProcentowa = rocznaStopaProcentowa;
    }
    static double rocznaStopaProcentowa;
    private double saldo;

    public static double getRocznaStopaProcentowa() {
        return rocznaStopaProcentowa;
    }

    public double getSaldo() {
        return saldo;
    }

    public void obliczMiesieczneOdsetki() {
        double miesOdsetki = (saldo * rocznaStopaProcentowa)/12;
        saldo+=miesOdsetki;

    }
    public static void setRocznaStopaProcentowa(double nowa){
        rocznaStopaProcentowa = nowa;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
