package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File tekst = new File("text");
        Scanner scan = new Scanner(tekst);
        Map<Integer, HashSet<String>> wiersz = new HashMap<>();
        HashSet<String> wyrazy = new HashSet<>();
        while(scan.hasNext())
        {
            String wynik = scan.next();
            String[] slowa = wynik.split(" ");
            HashSet<String> tmp = new HashSet<>();
            for(int j=0;j< slowa.length;j++)
            {
                for (int i = 0; i < tmp.size(); i++)
                {
                    if (!tmp.contains(slowa[j]))
                    {
                        tmp.add(slowa[i]);
                    }
                }
                wiersz.put(slowa[j].hashCode(),tmp);
            }
        }
        for(int i=0;i<wiersz.size();i++)
        {
            System.out.print(wiersz.keySet().toArray()[i]);
            System.out.print(" : ");
            System.out.print(wiersz.get(wiersz.keySet().toArray()[i]).size());
            System.out.println();
        }



    }
    public static HashSet<String> lista(String slowo)
    {
        HashSet<String> wynik = new HashSet<>();
        wynik.add(slowo);
        return wynik;
    }
}
