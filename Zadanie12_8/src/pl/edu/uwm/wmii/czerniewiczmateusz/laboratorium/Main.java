package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> liczby = new ArrayList<>();
        ArrayList<String> slowa = new ArrayList<>();
        liczby.add(1);
        liczby.add(2);
        liczby.add(3);
        slowa.add("Ala");
        slowa.add("ma");
        slowa.add("kota");
        print(liczby);
        System.out.println();
        print(slowa);
    }
    public static<T extends Iterable> void print(T a ){
        a.forEach((item) -> System.out.print(item+", "));
    }
    {

    }

}
