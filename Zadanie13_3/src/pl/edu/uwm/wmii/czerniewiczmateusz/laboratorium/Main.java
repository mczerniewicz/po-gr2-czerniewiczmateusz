package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<Student, char[]> stud = new TreeMap<>();
        System.out.println("Witaj w liście studentów. Wpisz odpowiednie komendy aby wykonać akcje:");
        System.out.println("zakończ - kończy program");
        System.out.println("dodaj - dodaje studenta");
        System.out.println("usuń - usuwa studenta o podanym nazwisku");
        System.out.println("zmień - zmienia ocenę studentowi o podanym nazwisku");
        System.out.println("wypisz - wypisuje listę studentów");
        String odpowiedz = "odp";

        while (!odpowiedz.equals("zakończ")) {
            odpowiedz = scan.nextLine();
            switch (odpowiedz) {
                case "dodaj":

                    System.out.println("Podaj nazwisko: ");
                    String nazwisko = scan.next();
                    System.out.println("Podaj imie: ");
                    String imie = scan.next();
                    System.out.println("Podaj id: ");
                    int id = scan.nextInt();
                    System.out.println("Podaj ocene: ");
                    String ocena = scan.next();
                    Student st = new Student(imie, nazwisko,id);
                    stud.put(st, ocena.toCharArray());
                    odpowiedz = "odp";
                    System.out.println("Dodano " + nazwisko + " " + ocena);

                    break;
                case "usuń":

                    System.out.print("Podaj id: ");
                    int identyfikator = scan.nextInt();
                    stud.remove(identyfikator);
                    System.out.println(identyfikator);
                    odpowiedz = "odp";
                    break;
                case "zmień":
                    System.out.println("Podaj nazwisko: ");
                    String n = scan.next();
                    wypisz(n.toCharArray());
                    System.out.print(" : ");
                    wypisz(stud.get(n));



                case "wypisz":

                    Set<Student> nazwiska = stud.keySet();

                    odpowiedz = "odp";
                    break;
                case "zakończ":
                    break;

            }
        }
    }
    public static void wypisz(char[] a){
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i]);
        }
    }
}

