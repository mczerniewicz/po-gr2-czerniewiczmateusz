package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Student {
    public Student(String imie, String nazwisko, int id)
    {
        this.id=id;
        this.imie=imie;
        this.nazwisko = nazwisko;
    }
    String imie;
    String nazwisko;
    int id;

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getId() {
        return id;
    }
}
