package pl.imiajd.czerniewicz;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Osoba implements Cloneable, Comparable<Osoba>{

    public Osoba(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public String toString() {
        return "Osoba[" +
                "nazwisko='" + nazwisko + '\'' +
                ", dataUrodzenia=" + dataUrodzenia.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return nazwisko.equals(osoba.nazwisko) &&
                dataUrodzenia.equals(osoba.dataUrodzenia);
    }

    @Override
    public int compareTo(Osoba other)
    {
        int wynik = this.nazwisko.compareTo(other.nazwisko);
        if(wynik==0)
        {
            wynik=this.dataUrodzenia.compareTo(other.dataUrodzenia);
        }
        return wynik;
    }



    private String nazwisko;
    private LocalDate dataUrodzenia;

}
