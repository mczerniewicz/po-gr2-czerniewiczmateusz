package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import pl.imiajd.czerniewicz.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class TestOsoba {

    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();
        {
            Osoba a = new Osoba("Domaszyński", LocalDate.of(2000, 10, 12));
            grupa.add(a);

            Osoba b = new Osoba("Albano", LocalDate.of(2000, 10, 12));
            grupa.add(b);

            Osoba c = new Osoba("Nowak", LocalDate.of(1995, 5, 16));
            grupa.add(c);

            Osoba d = new Osoba("Nowak", LocalDate.of(1975, 3, 28));
            grupa.add(d);

            Osoba e = new Osoba("Czerniewicz", LocalDate.of(2000, 2, 13));
            grupa.add(e);
        }
        for(int i=0;i<grupa.size();i++)
        {
            System.out.println(grupa.get(i).toString());
        }
        System.out.println("---------------------");
        grupa.sort(Osoba::compareTo);
        for(int i=0;i<grupa.size();i++)
        {
            System.out.println(grupa.get(i).toString());
        }



    }
}
