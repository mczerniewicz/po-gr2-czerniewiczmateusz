package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.*;
public class Main {

    public static void main(String[] args) {
	    Scanner scan = new Scanner(System.in);
	    int a;
	    int b;
	    System.out.println("Podaj A: ");
	    a=scan.nextInt();
	    System.out.println("Podaj B: ");
	    b=scan.nextInt();
	    System.out.println("Suma od A do B: " + suma(a,b));
    }
    public static int suma(int a, int b)
    {
        int wynik = 0;
        if(a>b)
        {
          for(int i=b;i<a+1;i++)
          {
              wynik+=i;
          }
        }
        if(a<b)
        {
            for(int i=a;i<b+1;i++)
            {
                wynik+=i;
            }
        }
        if(a==b)
        {
            return 0;
        }
        return wynik;
    }
}
