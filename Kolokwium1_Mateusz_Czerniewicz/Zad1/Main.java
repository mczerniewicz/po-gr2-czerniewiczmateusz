package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double b;
        double c;
        System.out.println("b*2x^2+2*x-3*c=0");

        System.out.println("Podaj współczynnik b: ");
        b = scan.nextInt();
        System.out.println("Podaj współczynnik c: ");
        c = scan.nextInt();
        double a = 4-4*(2*b)*(3*c);
        if(a<0)
        {
            System.out.println("Brak rozwiązań - alfa < 0");
        }
        if(a==0)
        {
            System.out.println("Jeden pierwiastek równy: ");
            System.out.println("x=" + ((-2)/(2*2*b)));
        }
        if(a>0)
        {
            System.out.println("Dwa pierwiastki równe: ");
            System.out.println("x1=" + (((-2)-Math.sqrt(a))/(2*2*b)) );
            System.out.println("x2=" + (((-2)+Math.sqrt(a))/(2*2*b)));
        }

    }
}
