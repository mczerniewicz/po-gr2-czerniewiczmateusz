package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.awt.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] a = new int[7];
        for(int i=0;i<7;i++)
        {
            System.out.println("Podaj " + (i+1) + " liczbę");
            a[i]=scan.nextInt();
        }
        System.out.println("Twoja tablica: ");
        wypisz(a);
        System.out.println("\nNowa tablica: ");
        wypisz(nieparzyste(a));

    }
    public static int[] nieparzyste(int[] a)
    {
        int[] wynik = a;
        for(int i=0;i<a.length;i++)
        {
            if(i%2==1)
            {
                wynik[i]=0;
            }
        }
        return wynik;
    }
    public static void wypisz(int[] a)
    {
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i]+" ");
        }
    }
}
