package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.*;
public class Zadanie4 {

    public static void main(String[] args) {
	    Scanner scan = new Scanner(System.in);
	    int[] a = new int[10];
	    for(int i=0;i<10;i++)
        {
            System.out.println("Podaj " + (i+1) + " liczbę:");
            a[i]=scan.nextInt();
        }
	    int mi = min_index(a);
	    if(mi==-1111)
        {
            System.out.println("Brak ujemnego elementu");
        }
	    else{
            System.out.println("Najmniejszy ujemny element: " + a[mi]);
            System.out.println("Index: " + mi);
        }



    }
    public static int min_index(int[] a)
    {
        int min = 0;
        int index=-1;
        for(int i=1;i<a.length;i++)
        {
            if(a[i]<0&&a[i]<min)
            {
                min=a[i];
                index=i;
            }
        }
        if(index == -1)
        {
            return -1111;
        }
        return index;
    }
}
