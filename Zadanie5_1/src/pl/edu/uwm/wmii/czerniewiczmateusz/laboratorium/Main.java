package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> a= create(5);
        ArrayList<Integer> b= create(5);
        System.out.println("a= "+a);
        System.out.println("b= "+b);
        System.out.println("a+b= "+append(a,b));
    }
    public static ArrayList<Integer> append(ArrayList<Integer> a,ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik= new ArrayList<Integer>();
        for(int i=0;i<a.size();i++)
        {
            wynik.add(a.get(i));
        }

        for(int j=0;j<b.size();j++)
        {
            wynik.add(b.get(j));
        }
        return wynik;
    }
    public static ArrayList<Integer> create(int dl)
    {
        ArrayList<Integer> wynik= new ArrayList<Integer>();
        for(int i=0;i<dl;i++)
        {
            wynik.add((int)(Math.random()*50));
        }
        return wynik;
    }

}
