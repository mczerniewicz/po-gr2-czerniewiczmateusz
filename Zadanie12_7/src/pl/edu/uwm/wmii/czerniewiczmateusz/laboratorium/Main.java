package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.ArrayList;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer> pierwsze = new ArrayList<>();
        ArrayList<Integer> primes = new ArrayList<>();
        int n = -1;
        while(n<2)
        {
            System.out.println("Podaj liczbę n (n>=2)");
            n = scan.nextInt();
        }
        //Tworzenie listy od 2 do n
        for(int i=2;i<=n;i++)
        {
            pierwsze.add(i);
        }

        //usuwanie elementów (zamiana na 0)
        System.out.println(pierwsze);
        for(int i=2;i<Math.sqrt(n);i++)
        {
            for(int j=i^2;j<pierwsze.size();j+=i)
            {
                if(pierwsze.get(j)!=i)
                {
                    pierwsze.set(j,0);
                }
            }
        }

        //przepisywanie elementów różnych od  0 z pierwsze do primes
        for(int i =0;i<pierwsze.size();i++)
        {
            if(pierwsze.get(i)!=0)
            {
                primes.add(pierwsze.get(i));
            }
        }
        System.out.println(primes);


    }
}
