package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

public class Main {

    public static void main(String[] args) {
        double[] tablica= new double[]{-3,-2,-1,0,1,2,3,4,5};
        int n = tablica.length;
        wypisz(ile(n,tablica));
    }
    public static void wypisz(int[] tab)
    {
        for(int i=0;i<tab.length;i++)
        {
            System.out.print(tab[i]+", ");
        }
    }
    public static int[] ile(int n, double[] tab){
        int minus=0;
        int plus=0;
        int zero=0;
        for(int i=0;i<n;i++)
        {
          if(tab[i]<0)
          {
              minus++;
          }
          if(tab[i]==0)
          {
              zero++;
          }
          if(tab[i]>0)
          {
              plus++;
          }

        }
        int[] wynik= new int[]{minus,zero,plus};
        return wynik;
    }
}
