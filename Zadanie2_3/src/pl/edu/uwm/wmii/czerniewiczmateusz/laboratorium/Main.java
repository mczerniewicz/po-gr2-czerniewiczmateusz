package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę m od 1 do 10:");
        int m = in.nextInt();
        System.out.println("Podaj liczbę n od 1 do 10:");
        int n = in.nextInt();
        System.out.println("Podaj liczbę k od 1 do 10:");
        int k = in.nextInt();

        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        generuj(a);
        generuj(b);
        System.out.println("Macierz a:");
        wypisz(a);
        System.out.println("Macierz b:");
        wypisz(b);
        int[][] c = new int[m][k];
        c=mnozenie_macierzy(a,b);
        wypisz(c);

    }
    public static void generuj(int[][] tab)
    {
        Random r = new Random();
        for(int i=0;i<tab.length;i++)
        {
            for(int j=0;j<tab[i].length;j++)
            {
                tab[i][j] = r.nextInt(10);
            }
        }
    }
    public static void wypisz(int[][] tab)
    {
        for(int i=0;i<tab.length;i++)
        {
            for(int j=0;j<tab[i].length;j++)
            {
                System.out.print(tab[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static int[][] mnozenie_macierzy(int[][] x,int[][] y)
    {
        int[][] wynik = new int[x.length][y[0].length];
        for(int i=0;i<x.length;i++)
        {
            for(int j=0;j<x[i].length;j++)
            {
              for(int k=0;k<y[j].length;k++)
              {
                  wynik[i][j]+=x[i][k]*y[k][j];
              }
            }
        }
        return wynik;
    }

}
