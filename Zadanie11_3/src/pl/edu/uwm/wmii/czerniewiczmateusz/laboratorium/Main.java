package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Integer[] cyfry1 = new Integer[]{1,2,3,4,5,6};
        Integer[] cyfry2 = new Integer[]{1,2,0,4,5,6};
        LocalDate a = LocalDate.of(2000,1,1);
        LocalDate b = LocalDate.of(2000,1,2);
        LocalDate c = LocalDate.of(2000,1,3);
        LocalDate[] daty1 = new LocalDate[]{a,b,c};
        LocalDate[] daty2 = new LocalDate[]{c,b,a};
        ArrayUtil test = new ArrayUtil();
        System.out.println(test.isSorted(cyfry1));
        System.out.println(test.isSorted(cyfry2));
        System.out.println(test.isSorted(daty1));
        System.out.println(test.isSorted(daty2));






    }
}
