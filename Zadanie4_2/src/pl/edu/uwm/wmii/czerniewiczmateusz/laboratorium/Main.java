package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.*;
public class Main {

    public static void main(String[] args) throws FileNotFoundException{
	    File obj = new File("test.txt");
	    Scanner in = new Scanner(obj);
	    String file = new String("");
	    System.out.println(countChar(in,file,'a'));

    }
    public static int countChar(Scanner in, String str,char c)
    {
        int count=0;
        while(in.hasNextLine())
        {
            str = in.nextLine();
            for(int i=0;i<str.length();i++)
            {
                if(str.charAt(i)==c)
                {
                    count++;
                }
            }
        }
        return count;
    }
}
