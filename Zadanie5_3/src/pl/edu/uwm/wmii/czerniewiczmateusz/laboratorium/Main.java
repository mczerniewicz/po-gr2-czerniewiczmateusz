package pl.edu.uwm.wmii.czerniewiczmateusz.laboratorium;

import javax.lang.model.type.NullType;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> a= create(5);
        ArrayList<Integer> b= create(5);
        System.out.println("a= "+a);
        System.out.println("b= "+b);
        System.out.println("a+b= "+mergeSorted(a,b));
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a,ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        ArrayList<Integer> tmpa = a;
        ArrayList<Integer> tmpb = b;
        Collections.sort(tmpa);
        Collections.sort(tmpb);

        int d = a.size()+b.size();
        for(int i=0;i<d;i++)
        {
            if(tmpa.size()==0 && tmpb.size()!=0)
            {
                int n = tmpb.get(0);
                for (int k = 0; k < tmpb.size(); k++)
                {
                    n = min(n, tmpb.get(k));
                }
                wynik.add(n);
                tmpb.remove(tmpb.indexOf(n));
            }
            if(tmpb.size()==0 && tmpa.size()!=0)
            {
                int m = tmpa.get(0);
                for (int k = 0; k < tmpa.size(); k++) {
                    m = min(m, tmpa.get(k));
                }
                wynik.add(m);
                tmpa.remove(tmpa.indexOf(m));

            }
            if(tmpa.size()!=0 && tmpb.size()!=0)
            {
                int m = tmpa.get(0);
                for (int k = 0; k < tmpa.size(); k++) {
                    m = min(m, tmpa.get(k));
                }
                int n = tmpb.get(0);
                for (int k = 0; k < tmpb.size(); k++) {
                    n = min(n, tmpb.get(k));
                }
                wynik.add(min(m, n));
                if (min(m, n) == m) {
                    tmpa.remove(tmpa.indexOf(m));
                } else {
                    tmpb.remove(tmpb.indexOf(n));
                }
            }
        }

        return wynik;
    }
    public static ArrayList<Integer> create(int dl)
    {
        ArrayList<Integer> wynik= new ArrayList<Integer>();
        for(int i=0;i<dl;i++)
        {
            wynik.add((int)(Math.random()*50));
        }
        return wynik;
    }
    public static int min(int a, int b)
    {
        if(a>b)
        {
            return b;
        }
        else{
            return a;
        }
    }


}
